/**
 * DogService
 *
 * @description :: Service for managing Dogs
 */

module.exports = {
   
    getDog: function (dogId, next) {
        Dog.findOne({ 'id': dogId }).exec(function (err, record) {
            if (err) {
                next(err, record);
            }
            next(null, record);
        });
    },

    getAll: function (next) {
        Dog.find().exec(function (err, record) {
            if (err) {
                next(err, record);
            }
            next(null, record);
        });
    },
}