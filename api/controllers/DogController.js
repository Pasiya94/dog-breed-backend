/**
 * DogsController
 *
 * @description :: Server-side logic for managing dogs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


    getAll: function (req, res) {

        DogService.getAll(function (err, user) {
            if (err) {
                return res.serverError({
                    error: err
                });
            }
            return res.ok(user);
        });
    },

    getDog: function (req, res) {

        var dogId = req.param('id');

        DogService.getDog(dogId, function (err, dog) {
            if (err) {
                return res.serverError({
                    error: err
                });
            }

            if (!dog) {
                return res.badRequest();
            }

            return res.ok(dog);

        });
    },

};

